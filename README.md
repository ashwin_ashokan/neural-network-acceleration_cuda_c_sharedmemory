# Neural Network Acceleration_CUDA_C_SharedMemory

This project Uses CUDA_C NVIDIA library Framework to accelerate the BPN(training phase) of the NN by parallelizing its individual threads and running it across multiple CUDA Cores, and taking advantage of the large shared memory.
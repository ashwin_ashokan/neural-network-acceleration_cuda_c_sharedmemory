#include"NeuralNetwork.h"
void NeuralNetwork::setCurrentInput(vector <double> input)
{
	this->input = input;

	for (int i = 0; i < input.size(); i++)
	{
		this->layers.at(0)->setVal(i, input.at(i));
	}
}


NeuralNetwork::NeuralNetwork(vector <int> topology, double bias, double learningRate, double momentum)
{
	this->topology = topology;
	this->topologySize = topology.size();
	this->learningRate = learningRate;
	this->bias = bias;

	for (int i = 0; i < topologySize; i++)
	{

		if (i>0 && i < (topologySize - 1))
		{
			this->layers.push_back(new Layer(topology.at(i), this->hiddenActivationType));

		}
		else if (i == (topologySize - 1))
		{
			this->layers.push_back(new Layer(topology.at(i), this->outputActivationType));
		}
		else
		{
			this->layers.push_back(new Layer(topology.at(i)));
		}
	}


	for (int i = 0; i < topologySize - 1; i++)
	{

		Matrix *weightMatrix = new Matrix(topology.at(i), topology.at(i + 1), true);
		this->weightMatrices.push_back(weightMatrix);
	}

	for (int i = 0; i < (this->topology.at(topology.size() - 1)); i++)
	{
		errors.push_back(0.00);
		derivedErrors.push_back(0.00);

	}

	this->error = 0.00;
}

NeuralNetwork::NeuralNetwork(vector <int> topology, int hiddenActivationType, int outputActivationType, int costFunctionType, double bias, double learningRate, double momentum)
{

	this->topology = topology;
	this->topologySize = topology.size();
	this->learningRate = learningRate;
	this->bias = bias;

	this->hiddenActivationType = hiddenActivationType;
	this->outputActivationType = outputActivationType;
	this->costFunctionType = costFunctionType;

	//cout<<"TopologySize = "<<topologySize<<endl;

	for (int i = 0; i < topologySize; i++)
	{

		if (i>0 && i < (topologySize - 1))
		{
			this->layers.push_back(new Layer(topology.at(i), this->hiddenActivationType));

		}
		else if (i == (topologySize - 1))
		{
			this->layers.push_back(new Layer(topology.at(i), this->outputActivationType));

		}
		else
		{
			this->layers.push_back(new Layer(topology.at(i)));
		}

	}


	for (int i = 0; i < topologySize - 1; i++)
	{

		Matrix *weightMatrix = new Matrix(topology.at(i), topology.at(i + 1), true);
		//weightMatrix->PrintToConsole();//printing
		//cout<<"________________________________"<<endl;
		this->weightMatrices.push_back(weightMatrix);
	}

	for (int i = 0; i < (this->topology.at(topology.size() - 1)); i++)
	{
		errors.push_back(0.00);
		derivedErrors.push_back(0.00);

	}

	this->error = 0.00;
	//cout<<"Hello"<<endl;
	//cout<<this->topology.at(topology.size() - 1)<<endl;
	//cout<<"TopologySize = "<<topologySize<<endl;
	//cout<< "ERROR: " <<this->topology.at((topology.size()-1))<<endl;
	//cout<< "ERROR: " << this -> layers.at(this->layers.size() - 1)->getNeurons().size()<<endl;
}



#include"NeuralNetwork.h"
#include<assert.h>
void NeuralNetwork::setErrors()
{

	if (this->target.size() == 0)
	{
		cout << "here1" << endl;
		cerr << "No target for Neural Network" << endl;
		assert(false);
	}
	if (this->target.size() != this->layers.at(this->layers.size() - 1)->getNeurons().size())
	{
		cout << "here2" << endl;
		cerr << "Target size (" << this->target.size() << ") is not the same as ouptut layer size: " << this->layers.at(this->layers.size() - 1)->getNeurons().size() << endl;
		for (int i = 0; i < this->target.size(); i++)
		{
			cout << this->target.at(i) << endl;
		}

		assert(false);
	}


	switch (costFunctionType)
	{
	case (COST_MSE) :
	{
		//cout<<"here"<<endl;
		this->setErrorMSE();
		//cout<<"here"<<endl;
		break;
	}
	default:
	{

		this->setErrorMSE(); break;
	}
	}

}

void NeuralNetwork::setErrorMSE()
{
	int outputLayerIndex = this->layers.size() - 1;
	vector <Neuron *> outputNeurons = this->layers.at(outputLayerIndex)->getNeurons();

	this->error = 0.00;
	//Here GPU can be used to bring slight Time increase
	for (int i = 0; i < target.size(); i++)
	{
		double t = target.at(i);
		double y = outputNeurons.at(i)->getActivatedVal();

		this->errors.at(i) = 0.5 * pow(abs((t - y)), 2);

		this->derivedErrors.at(i) = (y - t);

		this->error += errors.at(i);

	}
}
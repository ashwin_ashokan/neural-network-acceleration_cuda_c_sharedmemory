#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<vector>
#include<stdio.h>
#include<fstream>
#include<streambuf>
#include<ostream>
#include<chrono>
#include<stdlib.h>
#define epoch 100
#define noOfPixels 784
#define trainingDataSize 50
#define HiddenLayerNeurons 30
#define OutputLayerNeurons 10
#define InputLayerNeurons 784
#define testingDataSize 10000
#define samplingSize 50
#include"NeuralNetwork.h"
using namespace std;


int main(int argc, char **argv)
{
	ofstream ErrorData;
	ErrorData.open("ErrorData.csv");
	vector<int> topology;
	topology.push_back(InputLayerNeurons);
	topology.push_back(HiddenLayerNeurons);
	topology.push_back(OutputLayerNeurons);
	double learningRate = 0.18;
	double momentum = 1;
	double bias = 1;
	vector<double> input;
	vector<double> target;
	FILE *fptr;
	double averageMSE = 0;
	NeuralNetwork *n = new NeuralNetwork(topology, 2, 3, 1, 1, 0.05, 1);

	for (unsigned epochs = 0; epochs <epoch; ++epochs)
	{

		fptr = fopen("C:\\Users\\ashwin\\Downloads\\CSIR 2018\\mnist_train.csv", "r");
		if (fptr == NULL) {
			cout << "File Could Not be Opened" << endl;
			exit(EXIT_SUCCESS);
		}
		else if (epochs == 0) cout << "File sucessfully Opened" << endl;

		cout << "Epoch Number: " << epochs << endl;
		int label;



		for (unsigned noOfCharacters = 0; noOfCharacters <trainingDataSize; ++noOfCharacters)
		{

			input.clear();
			target.clear();
			fscanf(fptr, "%i", &label);
			for (unsigned i = 0; i < 10; i++)
			{
				if (i == label) target.push_back(1);
				else target.push_back(0);

			}
			char s;
			int temp;
			double tempDouble;
			for (unsigned pixelData = 0; pixelData <noOfPixels; pixelData++)
			{
				fscanf(fptr, "%c", &s);
				fscanf(fptr, "%i", &temp);
				tempDouble = (double(temp) / 255);
				input.push_back(tempDouble);

			}

			n->train(input, target, bias, learningRate, momentum);
			averageMSE += n->error;

			if ((noOfCharacters + 1) % 50 == 0)
			{
				cout << "Average Error over " << samplingSize << " samples : " << averageMSE / samplingSize << endl;
				averageMSE = 0;
			}

		}

	}
	cout << " Finished Training Dataset " << endl;

	//##############################Testing Images#############################################

	cout << "Started Testing Images with The trained Neural Net" << endl;





	fptr = fopen("C:\\Users\\ashwin\\Downloads\\CSIR2018\\Notes_Dataset\\mnist_test_10.csv", "r");
	if (fptr == NULL) {
		cout << "File Could Not be Opened" << endl;
		exit(EXIT_SUCCESS);
	}
	cout << "File sucessfully Opened" << endl;
	int label;
	int greatest = 0;
	int count = 0;
	for (unsigned noOfCharacters = 0; noOfCharacters < testingDataSize; ++noOfCharacters)
	{

		input.clear();
		target.clear();
		fscanf(fptr, "%i", &label);
		for (unsigned i = 0; i < 10; i++)
		{
			if (i == label) target.push_back(1);
			else target.push_back(0);
		}

		char s;
		int temp;
		double tempDouble;
		for (unsigned pixelData = 0; pixelData < noOfPixels; pixelData++)
		{
			fscanf(fptr, "%c", &s);
			fscanf(fptr, "%i", &temp);
			tempDouble = (double(temp) / 255);
			input.push_back(tempDouble);
		}


		n->setCurrentInput(input);
		n->feedForward();

		for (int k = 0; k < topology.at(topology.size() - 1); k++)
		{
			if (n->layers.at(topology.size() - 1)->getNeurons().at(k)->getActivatedVal() > n->layers.at(topology.size() - 1)->getNeurons().at(greatest)->getActivatedVal())
			{
				greatest = k;
			}


		}

		if (label == greatest)
		{
			count++;
		}
	}
	cout << "No of Correct Detection = " << count << endl;
	count = 0;





	getchar();
	return 0;
}

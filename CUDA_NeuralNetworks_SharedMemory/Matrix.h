#ifndef _MATRIX_HPP_
#define _MATRIX_HPP_

#include <iostream>
#include <vector>
#include <random>
#include <iomanip>
#include<chrono>
#include<iostream>

using namespace std;

class Matrix
{
public:
	Matrix(int numRows, int numCols, bool isRandom);

	Matrix *transpose();
	Matrix *copy();

	void setValue(int r, int c, double v) { this->values.at(r*this->numCols +c) = v; }
	double getValue(int r, int c) { return this->values.at(r*this->numCols + c); }


	//changed to vector<double> from former
	vector<double>  getValues() { return this->values; }

	void printToConsole();

	int getNumRows() { return this->numRows; }
	int getNumCols() { return this->numCols; }
	double * getVectorAddress() { return this->values.data(); }
private:
	double generateRandomNumber();

	int numRows;
	int numCols;
	
	//changed
	vector<double> values;
};

#endif
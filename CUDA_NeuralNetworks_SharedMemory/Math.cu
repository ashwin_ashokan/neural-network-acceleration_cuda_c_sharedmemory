#include "Math.h"
#include<cuda.h>
#include<device_launch_parameters.h>
#include<cuda_runtime.h>
#define TITLE_WIDTH 16

__global__ void matrixMulSharedMemory(double *a, double*b, double*c, int aRows, int aCols, int bRows, int bCols)
{
	int Row = blockIdx.y*blockDim.y + threadIdx.y;
	int Col = blockIdx.x*blockDim.x + threadIdx.x;

		
			__shared__ double Mds[TITLE_WIDTH][TITLE_WIDTH];
			__shared__ double Nds[TITLE_WIDTH][TITLE_WIDTH];

			double temp_value = 0;

			for (int m = 0; m < (aCols + TITLE_WIDTH - 1) / TITLE_WIDTH; ++m)
			{
		
					if (m*TITLE_WIDTH + threadIdx.x < aCols  &&  Row < aRows)
					{
						Mds[threadIdx.y][threadIdx.x] = a[Row*aCols + m*TITLE_WIDTH + threadIdx.x];
					}
					else Mds[threadIdx.y][threadIdx.x] = 0;

					if (m*TITLE_WIDTH + threadIdx.y < bRows && Col < bCols)
					{
						Nds[threadIdx.y][threadIdx.x] = b[(m*TITLE_WIDTH + threadIdx.y)*bCols + Col];
					}
					else Nds[threadIdx.y][threadIdx.x] = 0;
					
					__syncthreads();
					
					for (int k = 0; k < TITLE_WIDTH; ++k)
					{
						temp_value += Mds[threadIdx.y][k] * Nds[k][threadIdx.x];
					}
					__syncthreads();
				}
			

			if (Row < aRows && Col < bCols) c[Row*bCols + Col] = temp_value;

	}



void utils::Math::multiplyMatrix(Matrix *a, Matrix *b, Matrix *c)
{
	//Obtaining size of Rows and cols of Input matrix

	
	
	int aRows = a->getNumRows();
	int aCols = a->getNumCols();
	int bRows = b->getNumRows();
	int bCols = b->getNumCols();
	
	
	
	//Create Device Pointer 
	double *matA_dev, *matB_dev, *matC_dev;
	
	
	//Allocation of memory for the matrices in device
	cudaMalloc(&matA_dev, sizeof(double)*aRows*aCols);
	cudaMalloc(&matB_dev, sizeof(double)*bRows*bCols);
	cudaMalloc(&matC_dev, sizeof(double)*aRows*bCols);

	//Transferring data from host to device
	cudaMemcpy(matA_dev,a->getVectorAddress(), sizeof(double)*aRows*aCols, cudaMemcpyHostToDevice);
	cudaMemcpy(matB_dev, b->getVectorAddress(), sizeof(double)*bRows*bCols, cudaMemcpyHostToDevice);
	
	//	determining the size of blocks and threads
	dim3 blockDimension(TITLE_WIDTH, TITLE_WIDTH,1);
	dim3 gridDimension((aCols > bCols ? aCols : bCols) / TITLE_WIDTH+ 1, (aRows > bRows ? aRows : bRows) / TITLE_WIDTH + 1, 1);

	matrixMulSharedMemory << <gridDimension, blockDimension >> >(matA_dev, matB_dev, matC_dev, aRows, aCols, bRows, bCols);
	cudaDeviceSynchronize();
	cudaMemcpy(c->getVectorAddress(),matC_dev,sizeof(double)*aRows*bCols, cudaMemcpyDeviceToHost);

	//Freeing the device Memory
	cudaFree(matA_dev);
	cudaFree(matB_dev);
	cudaFree(matC_dev);

	

}
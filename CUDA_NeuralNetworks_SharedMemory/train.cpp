#include"NeuralNetwork.h"
#include<chrono>
void NeuralNetwork::train(
	vector<double> input,
	vector<double> target,
	double bias,
	double learningRate,
	double momentum
	) {
	this->learningRate = learningRate;
	this->momentum = momentum;
	this->bias = bias;

	//chrono::system_clock::time_point start = chrono::system_clock::now();

	//setting Current Input For Large targets can Be parallelized
	this->setCurrentInput(input);
	//setting Current Target Will Bring very miniute time difference
	this->setCurrentTarget(target);
	this->feedForward();
	this->setErrors();
	//significant time Reduction can be made o
	this->backPropagation();
	//chrono::system_clock::time_point end = chrono::system_clock::now();
	//chrono::duration<double> estimate = end - start;
	//cout << "Training Time in GPU V 1.0.1 : " << estimate.count() << "s" << endl;


}